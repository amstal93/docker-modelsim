# Copyright 2020 Tymoteusz Blazejczyk
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Basic image
ARG CENTOS_VERSION=8.1.1911
FROM centos:${CENTOS_VERSION} as base

# Install packages
FROM base as builder

RUN \
    dnf --enablerepo=PowerTools install --assumeyes \
        bzip2 \
        gcc \
        make \
        uuid \
        which \
        gperf \
        diffutils \
        libgcc.i686 \
        libpng-devel \
        libuuid-devel \
        libxml2-devel \
        glibc-devel.i686 \
        libpng-devel.i686 \
        libuuid-devel.i686 \
        libxml2-devel.i686 \
        && \
    dnf clean all && \
    rm -rf /var/cache/dnf/*

# Download, build and install the FreeType package
FROM builder as freetype

ARG FREETYPE_URL=http://download.savannah.gnu.org/releases/freetype
ARG FREETYPE_VERSION=2.4.7

COPY cache/freetype-*.tar.bz2 .optional /tmp/

RUN \
    if [[ ! -f "/tmp/freetype-${FREETYPE_VERSION}.tar.bz2" ]]; then \
        curl \
            --location \
            --output "/tmp/freetype-${FREETYPE_VERSION}.tar.bz2" \
            "${FREETYPE_URL}/freetype-${FREETYPE_VERSION}.tar.bz2"; \
    fi && \
    mkdir -p /tmp/freetype /usr/local/include/freetype2/freetype/internal && \
    tar \
        --strip-components=1 \
        --directory=/tmp/freetype/ \
        --extract \
        --verbose \
        --file=/tmp/freetype-${FREETYPE_VERSION}.tar.bz2 \
        && \
    cd /tmp/freetype && \
    ./configure \
        --build=i686-pc-linux-gnu \
        --prefix=/usr/local/ \
        --disable-static \
        CFLAGS="-m32" \
        CXXFLAGS="-m32" \
        LDFLAGS="-m32" \
        && \
    make -j$(nproc) && \
    make install && \
    rm -rf /tmp/freetype*

# Download, build and install the FontConfig package
FROM builder as fontconfig

ARG FONTCONFIG_URL=https://www.freedesktop.org/software/fontconfig/release
ARG FONTCONFIG_VERSION=2.12.4

COPY --from=freetype /usr/local /usr/local/
COPY cache/fontconfig-*.tar.bz2 .optional /tmp/

RUN \
    if [[ ! -f "/tmp/fontconfig-${FONTCONFIG_VERSION}.tar.bz2" ]]; then \
        curl \
            --location \
            --output "/tmp/fontconfig-${FONTCONFIG_VERSION}.tar.bz2" \
            "${FONTCONFIG_URL}/fontconfig-${FONTCONFIG_VERSION}.tar.bz2"; \
    fi && \
    mkdir -p /tmp/fontconfig && \
    tar \
        --strip-components=1 \
        --directory=/tmp/fontconfig/ \
        --extract \
        --verbose \
        --file=/tmp/fontconfig-${FONTCONFIG_VERSION}.tar.bz2 \
        && \
    cd /tmp/fontconfig && \
    ./configure \
        --build=i686-pc-linux-gnu \
        --prefix=/usr/local/ \
        --enable-libxml2 \
        --disable-static \
        --disable-docs \
        CFLAGS="-m32" \
        CXXFLAGS="-m32" \
        LDFLAGS="-m32" \
        FREETYPE_LIBS="-L/usr/local/lib -lfreetype" \
        FREETYPE_CFLAGS="-I/usr/local/include -I/usr/local/include/freetype2" \
        && \
    make -j$(nproc) && \
    make install && \
    rm -rf /tmp/fontconfig*

# Download, build and install the ModelSim Intel FPGA Edition
FROM base as modelsim

ARG MODELSIM_INTEL_URL=http://download.altera.com/akdlm/software/acdsinst
ARG MODELSIM_INTEL_VERSION=19.1.0.670

COPY cache/ModelSim*Setup-*-linux.run .optional /tmp/

RUN \
    if ! ls /tmp/[Mm]odel[Ss]im* >/dev/null 2>&1; then \
        NAME="ModelSimSetup-${MODELSIM_INTEL_VERSION}-linux.run" && \
        MAJOR=$(echo "$MODELSIM_INTEL_VERSION" | cut -d '.' -f 1) && \
        MINOR=$(echo "$MODELSIM_INTEL_VERSION" | cut -d '.' -f 2) && \
        BUILD=$(echo "$MODELSIM_INTEL_VERSION" | cut -d '.' -f 4) && \
        URL="${MODELSIM_INTEL_URL}/${MAJOR:-0}.${MINOR:-0}std/${BUILD:-0}" && \
        URL="${URL}/ib_installers" && \
        curl \
            --location \
            --output /tmp/${NAME} \
            "${URL}/${NAME}"; \
    fi && \
    for run in $(ls /tmp/ModelSim*Setup-*-linux.run 2>/dev/null || true); do \
        chmod a+x "$run" && \
        $run \
            --unattendedmodeui minimal \
            --mode unattended \
            --installdir /opt/ \
            --accept_eula 1 \
            --modelsim_edition modelsim_ase; \
    done && \
    \
    if [[ -d /opt/modelsim_ae ]]; then \
        mv /opt/modelsim_ae /opt/modelsim; \
    fi && \
    \
    if [[ -d /opt/modelsim_ase ]]; then \
        mv /opt/modelsim_ase /opt/modelsim; \
    fi && \
    \
    if [[ -d /opt/modelsim/altera ]]; then \
        mv /opt/modelsim/altera /opt/altera; \
    fi && \
    \
    sed \
        --in-place \
        '/^\[msg_system\]$/a suppress = 3116' \
        /opt/modelsim/modelsim.ini \
        && \
    cd /opt/modelsim && ln -s linux linux_rh60 && cd - && \
    cd /opt/modelsim && ln -s linux linuxpe && cd - && \
    rm -rf \
        /opt/{logs,uninstall} \
        /opt/modelsim/{examples,docs} \
        /tmp/ModelSim*Setup-*-linux.run

# Build minimal image
FROM base AS minimal

ARG BUILD_DATE
ARG VCS_REF
ARG VERSION

# Build-time metadata as defined at http://label-schema.org
LABEL \
    maintainer="tymoteusz.blazejczyk@tymonx.com" \
    org.label-schema.schema-version="1.0" \
    org.label-schema.build-date=$BUILD_DATE \
    org.label-schema.name="tymonx/modelsim" \
    org.label-schema.description="A Docker image with the ModeSim HDL simulator" \
    org.label-schema.usage="https://gitlab.com/tymonx/docker-modelsim/README.md" \
    org.label-schema.url="https://gitlab.com/tymonx/docker-modelsim" \
    org.label-schema.vcs-url="https://gitlab.com/tymonx/docker-modelsim" \
    org.label-schema.vcs-ref=$VCS_REF \
    org.label-schema.vendor="tymonx" \
    org.label-schema.version=$VERSION \
    org.label-schema.docker.cmd=\
"docker run --rm --user $(id -u):$(id -g) --volume $(pwd):$(pwd) --workdir $(pwd) --entrypoint /bin/bash registry.gitlab.com/tymonx/docker-modelsim"

ENV \
    HOME=/home/modelsim \
    PATH=/opt/modelsim/bin:${PATH} \
    FONTCONFIG_PATH=/usr/local/etc/fonts \
    FONTCONFIG_FILE=/usr/local/etc/fonts/fonts.conf \
    MTI_VCO_MODE=32

RUN dnf install --assumeyes \
        glibc.i686 \
        libX11.i686 \
        libXft.i686 \
        libXext.i686 \
        libxml2.i686 \
        ncurses-devel.i686 \
        ncurses-compat-libs.i686 \
        && \
    echo "/usr/local/lib" > /etc/ld.so.conf.d/usr-local-lib.conf && \
    ldconfig && \
    dnf clean all && \
    rm -rf /var/cache/dnf/* && \
    adduser \
        --user-group \
        --no-log-init \
        --create-home \
        --shell /bin/bash \
        modelsim \
        && \
    chmod 777 /home/modelsim

COPY --from=freetype /usr/local /usr/local/
COPY --from=fontconfig /usr/local /usr/local/
COPY --from=modelsim /opt/modelsim /opt/modelsim/

WORKDIR /home/modelsim

# Build final image
FROM minimal

COPY --from=modelsim /opt/altera /opt/modelsim/altera/
